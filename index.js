/// <reference path="typings/node/node.d.ts"/>
/// <reference path="typings/express/express.d.ts"/>


var application_root = __dirname,
  express = require("express"),
  Sequelize = require('sequelize');

var sequelize = new Sequelize('feetrader_dev', 'root', '', {
  host: 'localhost',
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
  define: {
    timestamps: false // true by default
  }
});

var User = sequelize.define('users', {
  username: {
    type: Sequelize.STRING
  },
  firstName: {
    type: Sequelize.STRING,
    field: 'firstName' // Will result in an attribute that is firstName when user facing but first_name in the database
  },
  lastName: {
    type: Sequelize.STRING
  }
}, {
    freezeTableName: true // Model tableName will be the same as the model name
  });

var Job = sequelize.define('jobs', {
  title: {
    type: Sequelize.STRING
  }  
});

//User.hasMany(Job, { foreignKey: 'usersId' });
//Job.belongsTo(User, { foreignKey: 'usersId' });

var app = express();

app.get('/users', function (req, res) {
		User.findAll().then(function (users) {
      	res.send(JSON.stringify(users));
		});
});

app.get('/users/:id', function (req, res) {
		User.find(req.params.id).then(function (user) {
      	res.send(JSON.stringify(user));
		});
});

app.get('/users/:id/jobs', function (req, res) {
		Job.all({where:{usersId: req.params.id}}).then(function (jobs) {
      	res.send(JSON.stringify(jobs));
		});
});

app.get('/users/count', function (req, res) {
		User.count().then(function (c) {
        res.send("Hay " + c + " usuarios");
		});
});

app.get('/jobs', function(req, res) {
    Job.all().then(function(jobs) {
      res.send(JSON.stringify(jobs));
    });
});

app.listen(3000);